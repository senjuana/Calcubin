/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoramain;

import java.awt.Image;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 *
 * @author pingu
 */
public class CalculadoraMain extends JFrame{

    
    public static void main(String[] args) {
        
        try{
            javax.swing.UIManager.setLookAndFeel(
            javax.swing.UIManager.getSystemLookAndFeelClassName());
        }catch ( Exception e ) { 
        } 

        Calcular c = new Calcular();
        
        c.addKeyListener(c);
        c.setFocusable(true);
        
        c.setTitle("Calculadora");
        c.setResizable(false);
        c.setVisible(true);
        c.setLocationRelativeTo(null);
    }

    
}
