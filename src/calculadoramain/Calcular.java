
package calculadoramain;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.OutputStream;
import static java.lang.Math.sqrt;
import java.util.Enumeration;
import javax.swing.JOptionPane;

/**
 *
 * @author pingu
 */
public class Calcular extends javax.swing.JFrame implements KeyListener, ActionListener{
    boolean botonActivo=true, punto=true, cero=false, cero1=true;
    boolean Num1=true, Num2=false;
    boolean sumar=false, restar=false, multiplicar=false, dividir=false;
    boolean operador_resta=true, igual_aux=false;
    boolean max_pantalla=false, super_resultado=true;
    String numeros="", operacionVisible="";
    double x1=0, x2=0; //x1 = auxiliar para todas las operaciones
                       //x2 = auxiliar solo para porcentajes
  
    
    //conexion con arduino
    private OutputStream output=null;
    SerialPort serial;
    private final String Puerto="/dev/ttyUSB0"; //el puerto del arduino
    
    private static final int Timeout =2000;//milisegundos
    private static final int data_rate=9600;
    
    
    private void enviarDatos(String datos){
       try{
           output.write(datos.getBytes());
           
        }catch(Exception e){
            mostrarError("ERROR");
            System.exit(ERROR);
        }
   }
   
   public void mostrarError(String mensaje){
       JOptionPane.showMessageDialog(this,mensaje,"ERROR",JOptionPane.ERROR_MESSAGE);
   }
    
    
    
    
    public void initconexion(){
        CommPortIdentifier puertoId=null;
        Enumeration puertoEnum=CommPortIdentifier.getPortIdentifiers();
        
        while(puertoEnum.hasMoreElements()){
            CommPortIdentifier actualPuertoId =(CommPortIdentifier) puertoEnum.nextElement();
            if(Puerto.equals(actualPuertoId.getName())){
                puertoId=actualPuertoId;
                break;
            }
        }
        if(puertoId==null){
            mostrarError("No se pudo conectar al puerto");
            System.exit(ERROR);
        }
        
        try{
            serial = (SerialPort) puertoId.open(this.getClass().getName(),Timeout);
            //parametros de puerto serial
            serial.setSerialPortParams(data_rate, serial.DATABITS_8,serial.STOPBITS_1,serial.PARITY_NONE);
            
            output = serial.getOutputStream();
        }catch(Exception e){
            mostrarError(e.getMessage());
            System.exit(ERROR);
        }
            
    }
    
            
    public Calcular() {
        initComponents();
        Pantalla.setText("0");
        funcionBotones();
        initconexion();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Pantalla = new javax.swing.JTextField();
        botonClean = new javax.swing.JButton();
        botonCE = new javax.swing.JButton();
        botonPorcen = new javax.swing.JButton();
        botonDivi = new javax.swing.JButton();
        boton7 = new javax.swing.JButton();
        boton8 = new javax.swing.JButton();
        boton9 = new javax.swing.JButton();
        botonMulti = new javax.swing.JButton();
        boton4 = new javax.swing.JButton();
        boton5 = new javax.swing.JButton();
        boton6 = new javax.swing.JButton();
        botonResta = new javax.swing.JButton();
        boton1 = new javax.swing.JButton();
        boton2 = new javax.swing.JButton();
        boton3 = new javax.swing.JButton();
        botonSumar = new javax.swing.JButton();
        boton0 = new javax.swing.JButton();
        botonPunto = new javax.swing.JButton();
        botonIgual = new javax.swing.JButton();
        opeVisible = new javax.swing.JLabel();
        sobreButton = new javax.swing.JButton();
        raizButton = new javax.swing.JButton();
        instructivo = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Pantalla.setEditable(false);
        Pantalla.setBackground(new java.awt.Color(255, 255, 255));
        Pantalla.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        Pantalla.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        Pantalla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PantallaActionPerformed(evt);
            }
        });
        Pantalla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                PantallaKeyTyped(evt);
            }
        });

        botonClean.setBackground(new java.awt.Color(153, 153, 153));
        botonClean.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        botonClean.setText("C");
        botonClean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCleanActionPerformed(evt);
            }
        });

        botonCE.setBackground(new java.awt.Color(153, 153, 153));
        botonCE.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        botonCE.setText("<-");
        botonCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCEActionPerformed(evt);
            }
        });

        botonPorcen.setBackground(new java.awt.Color(153, 153, 153));
        botonPorcen.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        botonPorcen.setForeground(new java.awt.Color(204, 102, 0));
        botonPorcen.setText("%");
        botonPorcen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonPorcenActionPerformed(evt);
            }
        });

        botonDivi.setBackground(new java.awt.Color(153, 153, 153));
        botonDivi.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        botonDivi.setForeground(new java.awt.Color(0, 102, 204));
        botonDivi.setText("/");
        botonDivi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDiviActionPerformed(evt);
            }
        });

        boton7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton7.setText("7");
        boton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton7ActionPerformed(evt);
            }
        });

        boton8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton8.setText("8");
        boton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton8ActionPerformed(evt);
            }
        });

        boton9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton9.setText("9");
        boton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton9ActionPerformed(evt);
            }
        });

        botonMulti.setBackground(new java.awt.Color(153, 153, 153));
        botonMulti.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        botonMulti.setForeground(new java.awt.Color(0, 102, 204));
        botonMulti.setText("*");
        botonMulti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMultiActionPerformed(evt);
            }
        });

        boton4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton4.setText("4");
        boton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton4ActionPerformed(evt);
            }
        });

        boton5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton5.setText("5");
        boton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton5ActionPerformed(evt);
            }
        });

        boton6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton6.setText("6");
        boton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton6ActionPerformed(evt);
            }
        });

        botonResta.setBackground(new java.awt.Color(153, 153, 153));
        botonResta.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        botonResta.setForeground(new java.awt.Color(0, 102, 204));
        botonResta.setText("-");
        botonResta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRestaActionPerformed(evt);
            }
        });

        boton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton1.setText("1");
        boton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton1ActionPerformed(evt);
            }
        });

        boton2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton2.setText("2");
        boton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton2ActionPerformed(evt);
            }
        });

        boton3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton3.setText("3");
        boton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton3ActionPerformed(evt);
            }
        });

        botonSumar.setBackground(new java.awt.Color(153, 153, 153));
        botonSumar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        botonSumar.setForeground(new java.awt.Color(0, 102, 204));
        botonSumar.setText("+");
        botonSumar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSumarActionPerformed(evt);
            }
        });

        boton0.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        boton0.setText("0");
        boton0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton0ActionPerformed(evt);
            }
        });

        botonPunto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        botonPunto.setText(".");
        botonPunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonPuntoActionPerformed(evt);
            }
        });

        botonIgual.setBackground(new java.awt.Color(153, 153, 153));
        botonIgual.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        botonIgual.setForeground(new java.awt.Color(204, 102, 0));
        botonIgual.setText("=");
        botonIgual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonIgualActionPerformed(evt);
            }
        });

        opeVisible.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        opeVisible.setForeground(new java.awt.Color(102, 102, 102));
        opeVisible.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        sobreButton.setBackground(new java.awt.Color(153, 153, 153));
        sobreButton.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        sobreButton.setForeground(new java.awt.Color(204, 102, 0));
        sobreButton.setText("1/x");
        sobreButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sobreButtonActionPerformed(evt);
            }
        });

        raizButton.setBackground(new java.awt.Color(153, 153, 153));
        raizButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        raizButton.setForeground(new java.awt.Color(204, 102, 0));
        raizButton.setText("√");
        raizButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                raizButtonActionPerformed(evt);
            }
        });

        instructivo.setBackground(new java.awt.Color(153, 153, 153));
        instructivo.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        instructivo.setText("Ce");
        instructivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                instructivoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(boton4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(boton5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(boton6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botonMulti, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(boton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(boton2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(boton3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botonResta, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(boton0, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botonPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botonSumar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botonIgual, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(boton7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(boton8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(boton9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(instructivo, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(botonClean, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(botonDivi, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(botonPorcen, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(sobreButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(raizButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(botonCE, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(opeVisible, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Pantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(opeVisible, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Pantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonClean, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonCE, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(instructivo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(boton7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(boton8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(boton9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonDivi, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(boton4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(boton5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(boton6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonMulti, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(boton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(boton2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(boton3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonResta, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(boton0, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonIgual, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonSumar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(raizButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonPorcen, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sobreButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCEActionPerformed

        if(numeros.length()>0){
            numeros = numeros.substring(0, numeros.length()-1);
            Pantalla.setText(numeros);
            
            //comprueba en caso que se borre un ".", se vuelva a activar la opción
            //también comprueba si la primer variable es negativa 
            //(para activar operador resta)
            comprobarPunto();
        }
        if(Pantalla.getText().length()==0){
            Pantalla.setText("0");
            cero1=true;
            cero=false;
        }
        
    }//GEN-LAST:event_botonCEActionPerformed

    private void boton0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton0ActionPerformed
        double super_aux;
        compMax();
        if(max_pantalla==false){
        if(Num1){
            if(Pantalla.getText().contains(".")){
                    numeros = numeros + "0";
                    cero=true;
                    escribir();
            }
            if(cero1){
                    numeros = numeros + "0";
                    cero1=false;
                    escribir();
            }
            if(cero){
                if(Double.parseDouble(Pantalla.getText())!=0){
                    numeros = numeros + "0";
                    escribir();
                }
                
            }
        }
        if(Num2){
            if(cero1){
                numeros = numeros + "0";
                cero1=false;
                escribir();
            }
            if(cero){
                if(Double.parseDouble(Pantalla.getText())!=0){
                    numeros = numeros + "0";
                    escribir();
                }
                
            }
        }
        }
    }//GEN-LAST:event_boton0ActionPerformed

    private void botonPuntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonPuntoActionPerformed
    compMax();
        if(max_pantalla==false){
        if(Num1){
            if(punto){
                numeros = numeros + ".";
                escribir();
                punto=false;
            }
        }
        if(Num2){
            if(punto){
                numeros = numeros + ".";
                escribir();
                punto=false;
            }
        }
        }
    }//GEN-LAST:event_botonPuntoActionPerformed

    private void boton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton1ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "1";
            escribir();
        }
    }//GEN-LAST:event_boton1ActionPerformed

    private void boton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton2ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "2";
            escribir();
        }
    }//GEN-LAST:event_boton2ActionPerformed

    private void boton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton3ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "3";
            escribir();
        }
    }//GEN-LAST:event_boton3ActionPerformed

    private void boton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton4ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "4";
            escribir();
        }
    }//GEN-LAST:event_boton4ActionPerformed

    private void boton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton5ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "5";
            escribir();
        }
    }//GEN-LAST:event_boton5ActionPerformed

    private void boton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton6ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "6";
            escribir();
        }
    }//GEN-LAST:event_boton6ActionPerformed

    private void boton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton7ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "7";
            escribir();
        }
    }//GEN-LAST:event_boton7ActionPerformed

    private void boton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton8ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "8";
            escribir();
        }
    }//GEN-LAST:event_boton8ActionPerformed

    private void boton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton9ActionPerformed
        compMax();
        if(max_pantalla==false){
            numeros = numeros + "9";
            escribir();
        }
    }//GEN-LAST:event_boton9ActionPerformed

    private void botonSumarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSumarActionPerformed
    cero1=true;
    cero=false;
    
        try{
        if(Num1){
            if(numeros.length()!=0){ //si el usuario no ha introducido nada y presiona un operador lógico
                sumar=true;
                operacionVisible = operacionVisible + Pantalla.getText() + " + ";
                opeVisible.setText(operacionVisible);
                operacion();
            }
        }else{
            
        }
        }catch(NumberFormatException ex){
                igual_aux=true;
                Pantalla.setText("Error! /( .___.)/");
        }
    }//GEN-LAST:event_botonSumarActionPerformed

    private void botonRestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRestaActionPerformed
        if(Num1){
            try{
            if(numeros.length()!=0){ //si el usuario no ha introducido nada y presiona un operador lógico
                cero1=true;
                cero=false;
                restar=true;
                operacionVisible = operacionVisible + Pantalla.getText() + " - ";
                opeVisible.setText(operacionVisible);
                operacion();
            }}catch(NumberFormatException ex){
                igual_aux=true;
                Pantalla.setText("Error! /( .___.)/");
            }
            
            
            if(operador_resta==true){ //si el primer operador es un número negativo
                numeros = numeros + "-";
                operador_resta=false;
                Pantalla.setText(numeros);
            }
        }else{
            
        }
    }//GEN-LAST:event_botonRestaActionPerformed

    private void botonMultiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMultiActionPerformed
        cero1=true;
        cero=false;
        try{
        if(Num1){
            if(numeros.length()!=0){ //si el usuario no ha introducido nada y presiona un operador lógico
                multiplicar=true;
                operacionVisible = operacionVisible + Pantalla.getText() + " * ";
                opeVisible.setText(operacionVisible);
                operacion();
            }
        }else{
            
        }
        }catch(NumberFormatException ex){
                igual_aux=true;
                Pantalla.setText("Error! /( .___.)/");
        }
    }//GEN-LAST:event_botonMultiActionPerformed

    private void botonDiviActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDiviActionPerformed
        cero1=true;
        cero=false;
        
        try{
        if(Num1){
            if(numeros.length()!=0){ //si el usuario no ha introducido nada y presiona un operador lógico
                dividir=true;
                operacionVisible = operacionVisible + Pantalla.getText() + " / ";
                opeVisible.setText(operacionVisible);
                operacion();
            }
        }else{
            
        }
        }catch(NumberFormatException ex){
                igual_aux=true;
                Pantalla.setText("Error! /( .___.)/");
        }
    }//GEN-LAST:event_botonDiviActionPerformed

    private void botonPorcenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonPorcenActionPerformed
        cero1=true;
        cero=false;
        
        if(numeros.length()!=0){
        if(Num2==true && super_resultado==true){
            super_resultado=false;
            if(sumar){
                double resultado = (x1/100) + (Double.parseDouble(numeros));
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(restar){
                double resultado = (x1/100) - Double.parseDouble(numeros);
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(multiplicar){
                double resultado = (x1/100) * Double.parseDouble(numeros);
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(dividir){
                double resultado = (x1/100) / Double.parseDouble(numeros);
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
        }else{
            Pantalla.setText(numeros);
        }
    }else{
        Pantalla.setText("Error! /( .___.)/");
    }
    }//GEN-LAST:event_botonPorcenActionPerformed

    private void botonCleanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCleanActionPerformed
        punto=true;
        cero1=true;
        cero=false;
        Num1=true;
        Num2=false;
        sumar=false;
        restar=false;
        multiplicar=false;
        dividir=false;
        operador_resta=true;
        igual_aux=false;
        max_pantalla=false;
        super_resultado=true;
        numeros="";
        operacionVisible="";
        opeVisible.setText("");
        x1=0;
        x2=0;
        Pantalla.setText("0");
    }//GEN-LAST:event_botonCleanActionPerformed

    private void botonIgualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonIgualActionPerformed
    if(numeros.length()!=0){//si el usuario no ha introducido un segundo valor
        igual_aux=true;
        if(Num2==true && super_resultado==true){
            super_resultado=false;
            if(sumar){
                double resultado = x1 + (Double.parseDouble(numeros));
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                enviarDatos(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(restar){
                double resultado = x1 - Double.parseDouble(numeros);
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                enviarDatos(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(multiplicar){
                double resultado = x1 * Double.parseDouble(numeros);
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                //enviar operacion visible y splitear en arduino
                enviarDatos(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(dividir){
                double resultado = x1 / Double.parseDouble(numeros);
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                enviarDatos(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
        }else{
            Pantalla.setText(numeros);
        }
    }else{
        Pantalla.setText("Error! /( .___.)/");
    }
    }//GEN-LAST:event_botonIgualActionPerformed

    private void PantallaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PantallaActionPerformed
        
    }//GEN-LAST:event_PantallaActionPerformed

    private void PantallaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PantallaKeyTyped
        
    }//GEN-LAST:event_PantallaKeyTyped

    private void sobreButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sobreButtonActionPerformed
        cero1=true;
        cero=false;
        
        if(numeros.length()!=0){     
            igual_aux=true;
        if(Num2==true && super_resultado==true){
            super_resultado=false;
            if(sumar){
                double resultado = 1/(x1 + (Double.parseDouble(numeros)));
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(restar){
                double resultado = 1/(x1 + (Double.parseDouble(numeros)));
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(multiplicar){
                double resultado = 1/(x1 + (Double.parseDouble(numeros)));
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(dividir){
                double resultado = 1/(x1 + (Double.parseDouble(numeros)));
                operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
        }else{
            try{
                if((Double.parseDouble(Pantalla.getText().toString())) > 0){
                    double resultado = 1/(Double.parseDouble(numeros));
                    operacionVisible = operacionVisible + (Double.parseDouble(numeros));
                    opeVisible.setText(operacionVisible);
                    numeros = Double.toString(resultado);
                    Pantalla.setText(numeros);
                    comprobarPunto();
                }
            }catch(NumberFormatException ex){
                Pantalla.setText("Error! /( .___.)/");
        }
            
        }
    }else{
        Pantalla.setText("Error! /( .___.)/");
    }
    }//GEN-LAST:event_sobreButtonActionPerformed

    private void raizButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_raizButtonActionPerformed
        cero1=true;
        cero=false;
        
        if(numeros.length()!=0){     
            igual_aux=true;
        if(Num2==true && super_resultado==true){
            super_resultado=false;
            if(sumar){
                double resultado = sqrt((x1) + (Double.parseDouble(numeros)));
                operacionVisible = "√(" + operacionVisible + (Double.parseDouble(numeros)) + ")";
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(restar){
                double resultado = sqrt((x1) + (Double.parseDouble(numeros)));
                operacionVisible = "√(" + operacionVisible + (Double.parseDouble(numeros)) + ")";
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(multiplicar){
                double resultado = sqrt((x1) + (Double.parseDouble(numeros)));
                operacionVisible = "√(" + operacionVisible + (Double.parseDouble(numeros)) + ")";
                //aqui enviar la cadena de oepracion visible igual que en los demas :v
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
            if(dividir){
                double resultado = sqrt((x1) + (Double.parseDouble(numeros)));
                operacionVisible = "√(" + operacionVisible + (Double.parseDouble(numeros)) + ")";
                opeVisible.setText(operacionVisible);
                System.out.println(resultado);
                numeros = Double.toString(resultado);
                Pantalla.setText(numeros);
                comprobarPunto();
            }
        }else{
            try{
                if((Double.parseDouble(Pantalla.getText().toString())) > 0){
                    double resultado = sqrt(Double.parseDouble(numeros));
                    operacionVisible = "√(" + (Double.parseDouble(numeros)) + ")";
                    opeVisible.setText(operacionVisible);
                    numeros = Double.toString(resultado);
                    Pantalla.setText(numeros);
                    comprobarPunto();
                }
            }catch(NumberFormatException ex){
                Pantalla.setText("Error! /( .___.)/");
        }
            
        }
    }else{
        Pantalla.setText("Error! /( .___.)/");
    }
    }//GEN-LAST:event_raizButtonActionPerformed

    private void instructivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_instructivoActionPerformed
        JOptionPane.showMessageDialog(null, "Teclas: \nSupr = Limpiar operación\n" +
            "Del = Borrar último carácter\nShift + 7 = Divición\nShift + '+' = Multiplicación\n" +
            "'-' = Resta\n'+' = Suma\n'R' = Raíz\nShift + 5 = Porcentaje\n'S' = 1/x\n" +
            "Enter = Igual (resultado)");
    }//GEN-LAST:event_instructivoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calcular().setVisible(true);
            }

        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Pantalla;
    private javax.swing.JButton boton0;
    private javax.swing.JButton boton1;
    private javax.swing.JButton boton2;
    private javax.swing.JButton boton3;
    private javax.swing.JButton boton4;
    private javax.swing.JButton boton5;
    private javax.swing.JButton boton6;
    private javax.swing.JButton boton7;
    private javax.swing.JButton boton8;
    private javax.swing.JButton boton9;
    private javax.swing.JButton botonCE;
    private javax.swing.JButton botonClean;
    private javax.swing.JButton botonDivi;
    private javax.swing.JButton botonIgual;
    private javax.swing.JButton botonMulti;
    private javax.swing.JButton botonPorcen;
    private javax.swing.JButton botonPunto;
    private javax.swing.JButton botonResta;
    private javax.swing.JButton botonSumar;
    private javax.swing.JButton instructivo;
    private javax.swing.JLabel opeVisible;
    private javax.swing.JButton raizButton;
    private javax.swing.JButton sobreButton;
    // End of variables declaration//GEN-END:variables

    private void escribir() {
        
        if(Num1){
            operador_resta=false;
            cero=true;
            Pantalla.setText(numeros);
        }
        if(Num2){
            cero=true;
            operador_resta=false;
            Pantalla.setText(numeros);
        }
    }

    private void operacion() {
        x1 = Double.parseDouble(numeros);
        numeros="";
        Pantalla.setText(numeros);
        punto=true;
        cero=false;
        Num2=true;
        Num1=false;
    }

    private void comprobarPunto() {
        boolean puntoAux=false, restaAux=false;
        int contador=0;
            while(contador<=(numeros.length()-1)){
                char caracter = numeros.charAt(contador);
                if(caracter==46){ // "."
                    puntoAux=true;
                }
                if(caracter==45){ // "-"
                    restaAux=true;
                }
                contador++;
            }
            //si no se detectó un punto, sigue activa la opción de punto
            if(puntoAux==false){
                punto=true;
            }else{
                punto=false;
            }
            //si no se detectó un signo de resta, sigue activa la opción
            if(restaAux==false){
                operador_resta=true;
            }else{
                operador_resta=false;
            }
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //Operadores lógicos
        if(e.isShiftDown() && e.getKeyChar() == '?'){
            instructivo.doClick();
        }
        if(e.getKeyChar() == '+'){
            botonSumar.doClick();
        }
        if(e.getKeyChar() == '-'){
            botonResta.doClick();
        }
        if(e.isShiftDown() && e.getKeyChar() == '*'){
            botonMulti.doClick();
        }
        if(e.isShiftDown() && e.getKeyChar() == '/'){
            botonDivi.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_R){
            raizButton.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_S){
            sobreButton.doClick();
        }
        
        //Números
        if(e.getKeyCode() == KeyEvent.VK_0){
            boton0.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_1){
            boton1.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_2){
            boton2.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_3){
            boton3.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_4){
            boton4.doClick();
        }
        if(e.getKeyChar() == '5'){
            boton5.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_6){
            boton6.doClick();
        }
        if(e.getKeyChar() == '7'){
            boton7.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_8){
            boton8.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_9){
            boton9.doClick();
        }

        
        //Otros
        if((e.getKeyCode() == KeyEvent.VK_ENTER)){
            botonIgual.doClick();
        }
        if(e.isShiftDown() && e.getKeyChar() == '%'){
            botonPorcen.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE){
            if(igual_aux){
                botonClean.doClick();
            }else{
                botonCE.doClick();
            }
            
        }
        if(e.getKeyCode() == KeyEvent.VK_DELETE){
            botonClean.doClick();
        }
        if(e.getKeyCode() == KeyEvent.VK_PERIOD){
            botonPunto.doClick();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        requestFocusInWindow(true);
    }

    private void funcionBotones() {
        boton0.addActionListener(this);
        boton1.addActionListener(this);
        boton2.addActionListener(this);
        boton3.addActionListener(this);
        boton4.addActionListener(this);
        boton5.addActionListener(this);
        boton6.addActionListener(this);
        boton7.addActionListener(this);
        boton8.addActionListener(this);
        boton9.addActionListener(this);
        botonCE.addActionListener(this);
        botonClean.addActionListener(this);
        botonDivi.addActionListener(this);
        botonIgual.addActionListener(this);
        botonMulti.addActionListener(this);
        botonPorcen.addActionListener(this);
        botonPunto.addActionListener(this);
        botonResta.addActionListener(this);
        botonSumar.addActionListener(this);
        raizButton.addActionListener(this);
        sobreButton.addActionListener(this);
        opeVisible.addKeyListener(this);
        instructivo.addKeyListener(this);
        Pantalla.addKeyListener(this);
        
    }
    
    private void compMax() {
        int max_carac = Pantalla.getText().length();
        
        if(max_carac<=9){
            max_pantalla=false;
        }else{
            max_pantalla=true;
        }
    }


}
